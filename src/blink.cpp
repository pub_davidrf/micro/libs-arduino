#include "blink.h"
#include <Arduino.h>
#include "debug.h"

blink::blink(int gpio, int von, int voff){
  this->gpioLed = gpio;
  this->on      = von;
  this->off     = voff;

  pinMode(this->gpioLed, OUTPUT);

}
void blink::parpadea(int times, int milliSecs){
  int n = milliSecs/times;
  for (int i = 0; i < times; i = i + 1) {
    digitalWrite(this->gpioLed, this->on);
    delay(n);
    digitalWrite(this->gpioLed, this->off);
    if (i < (times-1)){
      delay(n);
    }
  }
}
void blink::On(){
  digitalWrite(this->gpioLed, this->on);
}
void blink::Off(){
  digitalWrite(this->gpioLed, this->off);
}

