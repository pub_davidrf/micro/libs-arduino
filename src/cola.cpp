#include "cola.h"
#include "fsvars.h"

cola::cola(String prefix){
  this->config = new fsvars(String("q_" + prefix));
  this->last = this->config->get(COLA_KEY_LAST,"0").toInt();
  this->first = this->config->get(COLA_KEY_FIRST,"0").toInt();
}

String cola::getLast(){
  return this->config->get(String(this->last),"");
}

String cola::getFirst(){
  if (this->first == 0){
    this->first = 1;
  }
  return this->config->get(String(this->first),"");
}

bool cola::deleteLast(){
  if (this->config->exists(String(this->last))){
    this->config->remove(String(this->last));
    this->last--;
    if (this->empty()){
      this->first = 0;
      this->last = 0;
      this->config->set(COLA_KEY_FIRST,String(this->first));
    }
    this->config->set(COLA_KEY_LAST,String(this->last));
    return true;
  }else{
    return false;
  }
}

bool cola::deleteFirst(){
  if (this->config->exists(String(this->first))){
    this->config->remove(String(this->first));
    this->first++;
    if (this->empty()){
      this->first = 0;
      this->last = 0;
      this->config->set(COLA_KEY_LAST,String(this->last));
    }
    this->config->set(COLA_KEY_FIRST,String(this->first));
    return true;
  }else{
    return false;
  }
}

bool cola::empty(){
  return (this->first > this->last);
}

uint cola::nvalues(){
  uint values = 0;
  if (this->first <= this->last){
    values = this->last - this->first + 1;
  }
  if ((this->first == 0)&&
    (this->last == 0)){
      values = 0;
    }
  /*
  Serial.print("COLA: Primero: ");
  Serial.print(this->first);
  Serial.print(" Ultimo: ");
  Serial.print(this->last);
  Serial.print(" NValores: ");
  Serial.println(values);
  */
  return values;
}

bool cola::add(String value){ // añade al final
  this->last++;
  if (this->last == this->first){
    Serial.println("He dado la vuelta. Error, el valor ultimo ya existe");
    this->last--;
    return false;
  }else{
    this->config->set(String(this->last), value);
    this->config->set(COLA_KEY_LAST,String(this->last));
    if (!this->config->exists(String(this->first))){
      this->first = this->last;
      this->config->set(COLA_KEY_FIRST,String(this->first));
    }
    return true;
  }
}

bool cola::insert(String value){ // añade al inicio
  this->first--;
  if (this->last == this->first){
    Serial.println("He dado la vuelta. Error, el primer valor ya existe");
    this->first++;
    return false;
  }else{
    this->config->set(String(this->first), value);
    this->config->set(COLA_KEY_FIRST,String(this->last));
    if (!this->config->exists(String(this->last))){
      this->last = this->first;
      this->config->set(COLA_KEY_FIRST,String(this->last));
    }
    return true;
  }
}