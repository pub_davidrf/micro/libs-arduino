#ifndef fsvars_h
#define fsvars_h

#include "FS.h"
class fsvars
{
  public:
    fsvars(String);
    String get(String, String);
    void set(String, String);
    void remove(String);
    bool exists(String);
  private:
    String prefix_var;
};
#endif