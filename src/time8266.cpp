#include "time8266.h"
#include <Arduino.h>
#include "fsvars.h"
#include <time.h>
#include "debug.h"

#define TZ_INFO "CET-1CEST,M3.5.0,M10.5.0/3"

time8266::time8266(unsigned long sleeptime){
  DEBUG_PRINTLN("Timezone: " + this->flashstore.get(String(TIME8266_TIMEZONE_KEY),"3600"));
  this->timeZone = this->flashstore.get(String(TIME8266_TIMEZONE_KEY),"3600").toInt();

  unsigned long compilationTime = UNIX_TIMESTAMP - this->timeZone + TIME8266_UPLOAD_SECONDS;
  unsigned long lastUpdate = (
    flashstore.get(
        TIME8266_FLASH_STORE_KEY,
      String(
        UNIX_TIMESTAMP
      )
    )
  ).toInt() + sleeptime;

  DEBUG_PRINTLN("time8266::INIT: compilationTime = " + String(compilationTime));
  DEBUG_PRINTLN("time8266::INIT: lastUpdate      = " + String(lastUpdate) );

  if (compilationTime > lastUpdate){
    DEBUG_PRINTLN("time8266::INIT: CompilationTime > lastUpdate");
    this->set(compilationTime);
  }else{
    DEBUG_PRINTLN("time8266::INIT: CompilationTime <= lastUpdate");
    this->set(lastUpdate);
  }
}

void time8266::set(unsigned long segundos) {

  static time_t now;
  struct tm *timeptr;
  char s[100];

  DEBUG_PRINTLN("time8266::SET: " + String(segundos) );
  this->h = segundos;
  this->hset = millis();
  this->persist();

  // Ajusta la zona horaria
  setenv("TZ", TZ_INFO, 1);
  tzset();

  // Ajusta la hora del sistema
  timeval epoch = {segundos, 0};
  const timeval *tv = &epoch;
  //timezone utc = {0,0};
  //const timezone *tz = &utc;
  //settimeofday(tv, tz);
  settimeofday(tv, nullptr);

}

void time8266::persist(void) {
  if ((this->persistTime + TIME8266_PERSIST_MILLIS) <= millis()){
    this->flashstore.set(String(TIME8266_FLASH_STORE_KEY),String( this->nowInternal() ));
    this->persistTime = millis();
  }
}

unsigned long time8266::nowInternal(void){
  unsigned int i=static_cast<unsigned int>((millis() - hset)/1000);
  unsigned long ahora = h  + i ;
  return ahora;
}

unsigned long time8266::now(void){
  this->persist();
  return this->nowInternal();
}

void time8266::setTimeZone(int horas){
  this->timeZone = horas * 3600;
  this->flashstore.set(String(TIME8266_TIMEZONE_KEY),String( this->timeZone ));
}
int time8266::getTimeZone(void){
  return this->timeZone / 3600;
}

String time8266::fecha(const char* mascara){
  int epoch_time = this->nowInternal() + this->timeZone;
  struct tm * timeinfo;
  time_t epoch_time_as_time_t = epoch_time;
  timeinfo = localtime(&epoch_time_as_time_t);
  char buffer[80];
  //strftime(buffer,sizeof(buffer),"%Y-%m-%d %H:%M:%S",timeinfo);
  strftime(buffer,sizeof(buffer),mascara,timeinfo);
  String s((const __FlashStringHelper*) buffer);
  this->persist();
  return s;
} 

// send an NTP request to the time server at the given address
unsigned long time8266::sendNTPpacket(IPAddress& address)
{
  //Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.begin(12300); //puerto local receptor de paquetes.
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

void time8266::getNtp(){
  if (this->ntpOk()){
    DEBUG_PRINTLN("time8266::getNtp Ya tengo respuesta reciente de NTP, no reintento");
    return;
  }
  bool dnsOk = WiFi.hostByName(ntpServerName, timeServerIP);
  if (!dnsOk){
    DEBUG_PRINTLN("time8266::getNtp No recibo DNS. Salgo y no vuelvo a intentarlo de momento");
    this->lastNtp = millis();
    return;
  }
  DEBUG_PRINT("time8266::GETNTP Pidiendo NTP a: ");
  DEBUG_PRINTLN(timeServerIP);
  sendNTPpacket(timeServerIP);
  delay(50);
  int cb = udp.parsePacket();
  if (cb) {
    DEBUG_PRINT("packet received, length=");
    DEBUG_PRINTLN(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;

    DEBUG_PRINT("Seconds since Jan 1 1900 = " );
    DEBUG_PRINTLN(secsSince1900);

    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:

    this->set(secsSince1900 - seventyYears);
    this->lastNtp = millis();
  }else{
    DEBUG_PRINTLN("Sin respuesta del ntp");
    DEBUG_PRINTLN(cb);
    this->lastNtp = millis();
  }
}

bool time8266::ntpOk(void){
  // DEBUG_PRINT("time8266::NTPOK ");
  // DEBUG_PRINT(this->lastNtp);
  // DEBUG_PRINT(" - ");
  // DEBUG_PRINTLN(millis());
  if ((this->lastNtp == 0) || (millis() < this->lastNtp)){
    this->lastNtp = 0;
    return false;
  }
  return (TIME8266_NTP_FRECUENCY_MILLIS + this->lastNtp) > millis();
}

unsigned long time8266::epoch(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second){
  unsigned long epochv = 0;


  uint16_t bisyear;
  int dias[] = {0,3,3,6,8,11,13,16,19,21,24,26,29};

  epochv += (year - 1970)     * SEC_PER_YEAR;
  epochv += (month - 1)       * SEC_PER_DAY * 28; // 28 dias por mes
  epochv += dias[(month - 1)] * SEC_PER_DAY; // dias que faltan 
  epochv += (day - 1)         * SEC_PER_DAY;
  epochv += hour              * SEC_PER_HOUR;
  epochv += minute            * SEC_PER_MIN;
  epochv += second;

  //Bisiestos
  if (month < 3){
    bisyear = year - 1;
  }else{
    bisyear = year;
  }
  epochv += ( int((bisyear - 1972) / 4) + 1 ) * SEC_PER_DAY;
  return epochv;
}