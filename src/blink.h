#ifndef blink_h
#define blink_h
class blink
{
  public:
    blink(int gpio, int von, int voff);
    void parpadea(int times, int milliSecs);
    void On();
    void Off();
  private:
    int gpioLed;
    int on;
    int off;
};
#endif