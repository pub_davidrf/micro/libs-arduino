#ifndef COLA_KEY_LAST
#define COLA_KEY_LAST "l"
#endif

#ifndef COLA_KEY_FIRST
#define COLA_KEY_FIRST "f"
#endif

#ifndef cola_h
#define cola_h
#include "fsvars.h"
class cola {
  public:
    cola(String prefix);
    String getLast();
    String getFirst();
    bool deleteLast();
    bool deleteFirst();
    bool add(String); // añade al final
    bool insert(String); // añade al inicio
    bool empty();
    uint nvalues();
  private:
    fsvars *config;
    unsigned int first;
    unsigned int last;
};
#endif