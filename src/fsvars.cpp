// ----------------------------- I N C L U D E S -------------------
#include "FS.h"
#ifndef ESP8266
#include "SPIFFS.h"
#endif
#include "fsvars.h"
#include "debug.h"

#ifndef FILE_READ
#define FILE_READ "r"
#endif

fsvars::fsvars(String prefix){
  prefix_var = "/" + prefix;
  SPIFFS.begin();
}

String fsvars::get(String name, String _default){

  String path = prefix_var + name;
  
  DEBUG_PRINTLN("fsvars::get PATH = '" + path + "'");

  if (!SPIFFS.exists(path)){
    DEBUG_PRINTLN("fsvars::get no existe: '" + path + "'");
    DEBUG_PRINTLN("fsvars::get DEFAULT: '" + _default + "'");
    this->set(name, _default);
    return _default;
  }

  File f = SPIFFS.open(path, FILE_READ);
  if (!f) {
    DEBUG_PRINTLN("fsvars::get error open DEFAULT: '" + _default + "'");
    return _default;
  }

  String Data;
  if (f.available()){
    Data = f.readStringUntil('\r');
  }
  f.close();
  DEBUG_PRINTLN("fsvars::get DATA: '" + Data + "'");
  return Data;
}

void fsvars::set(String name, String value){
  String path = prefix_var + name;

  File f = SPIFFS.open(path, "w");
  if (!f) {
    return;
  }
  f.println(value);
  f.close(); 
}

void fsvars::remove(String name){
  String path = prefix_var + name;
  if (!SPIFFS.exists(path)){
    SPIFFS.remove(path);
  }
}

bool fsvars::exists(String name){
  String path = prefix_var + name;
  return SPIFFS.exists(path);
}